import sys
import json
import logging
from collections import defaultdict

logger = logging.getLogger(__name__)

class FileNotFoundError(IOError):
    pass

def flatten_multilevel_json(d):
    """
    Flatten a JSON-formatted dictionary, which, with the BIRDSHOT project, 
    are typically multi-level. 

    Parameters
    ----------
    d : dict
        JSON-formatted dictionary, possible multi level, that is to be flattened.

    Returns
    -------
    dict[N-tuple, Union[str, float, int, bool, None]]
        Flattened JSON with N-tuple keys and scalar or list of scalars.
    """
    if isinstance(d, dict):
        return {
            (k,) + k2: v2
            for k,v in d.items()
            for k2,v2 in flatten_multilevel_json(v).items()
        }
    elif isinstance(d, list):
        # List of labeled values, each in their own dictionary
        tmp = defaultdict(list)
        for i,x in enumerate(d):
            # x: scalar value or dictionary
            if isinstance(x, dict):
                # d is list of dictionaries (labeled values)
                for k,v in flatten_multilevel_json(x).items():
                    # guarantees that x is in the form {tuple(*keys): non-dict value}
                    # tmp[k].append(v)
                    tmp[k + ("Value",)].append(v)
                    # tmp[("Index",)].append(i+1)
                    tmp[k + ("Index",)].append(i+1)
            elif isinstance(x, list):
                raise ValueError("Forms cannot handle lists of lists.")
            else:
                # d is list of scalars
                tmp[("Value",)].append(x)
                tmp[("Index",)].append(i+1)
        return tmp
    else:
        return {tuple(): d}

def read_node_link_for_digraph_app(filename):
    """Read a node-link JSON file"""
    with open(filename, 'rb') as ifs:
        logger.info("Opening file: {filename}")
        return json.load(ifs)

if __name__ == "__main__":
    try: 
        G = read_node_link_for_digraph_app(sys.argv[1])
    except FileNotFoundError as e:
        logger.error(f"File not found: {sys.argv[1]}")
        sys.exit(1)

    for node in G['nodes']:
        formFile = node["id"] + ".json"
        try:
            with open(formFile) as ifs:
                logger.info(f"Opened JSON form: {formFile}")
                form_data = json.load(ifs)
            flat = flatten_multilevel_json(form_data["data"])
            flat = {":".join(map(str, k)): flat[k] for k in flat.keys()}
            node["metadata"][0] = {**node["metadata"][0], **flat}
            logger.info(f"Processed JSON for node {node['id']}")
        except Exception as e:
            print(f"Error processing JSON for node {node['id']}: {e}")
            