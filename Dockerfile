FROM python:3.11-slim
RUN apt-get update \
    && apt-get install -y --no-install-recommends git \
    && apt-get purge -y --auto-remove \
    && rm -rf /var/lib/apt/lists/*

# Install package
COPY . /
RUN pip install -r requirements.txt

# Packages in the wheelhouse are local and must be installed from wheels.
RUN pip install wheelhouse/*.whl

# Setup working environment
# WORKDIR /
# CMD ["/bin/bash"]
WORKDIR /workdir
ENTRYPOINT [ "python" ]
