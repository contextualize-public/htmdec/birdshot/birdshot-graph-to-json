import sys
import json
import logging
import networkx as nx
import pickle

logger = logging.getLogger(__name__)

class FileNotFoundError(IOError):
    pass

def graph_to_node_link(graph):
    """
    Convert a NetworkX graph to a node-link JSON format.

    This function loads a NetworkX graph from a pickle file, 
    processes its nodes to match the format accepted by the
    Digraph app. The resulting JSON is saved in a file named
    'data.json' in the current working directory because this 
    will typically be a temporary, or intermediate, file.

    This function has a dependency on the carta_integrations
    package, available in the wheelhouse/*.whl file.

    Usage : python graph-to-node-link.py birdshot-graph.pkl

    Inputs
    ------
    graph file (str) : The input pickle file of the BIRDSHOT Campaign 1 graph.

    """
    try:
        with open(graph, 'rb') as ifs:
            logger.info(f"Opening file: {graph}")
            G = pickle.load(ifs)
    except FileNotFoundError as e:
        logger.error(f"File not found: {graph}")
        sys.exit(1)

    nodeMap = {node: node.id for node in G.nodes}
    newGraph = nx.relabel_nodes(G, nodeMap)

    for node in G.nodes:
        newGraph.nodes[node.id]["name"] = node.name
        newGraph.nodes[node.id]["info"] = None
        newGraph.nodes[node.id]["metadata"] = [{ "link": node.metadata["webViewLink"] }]

    data = nx.node_link_data(newGraph)

    with open('data.json', 'w') as ofs:
        json.dump(data, ofs)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        logger.error("Too many arguments specified. Usage: graph_to_node_link.py <input file>.")
        sys.exit(1)

    graph = sys.argv[1]
    graph_to_node_link(graph)
